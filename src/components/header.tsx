import * as React from "react";
import { useContext } from "react";
import chatContext from "./chatStoreContext";

const Header = props => {
  const Consumer = chatContext.Consumer;
  const { style } = useContext(chatContext);
  return (
    <div
      className="header-zxx"
      style={{
        backgroundColor:
          style.header && style.header.headerBackground != ""
            ? style.header.headerBackground
            : "white",
        color:
          style.header && style.header.textColor != ""
            ? style.header.textColor
            : "#424242",
        fontFamily: style.header && style.header.fontFamily ? style.header.fontFamily : ""
      }}
    >
      {style.header.image && <img src={style.header.image} className="picture-zxx" />}
      {style.header.title && <h3>{style.header.title}</h3>}
      {/* <div className="picture-zxx" /> */}
    </div>
  );
};

export default Header;
