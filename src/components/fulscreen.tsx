import * as React from "react";
import Header from "./header";
import ChatView from "./chatView";
import MessageBox from "./message";
import Context from "./chatStoreContext";
import interact from "interactjs";
import { Helmet } from "react-helmet";
import { style, chatMessage, IProps } from "../interfaces/types";
class MainContainer extends React.Component<IProps, any> {
  public updateChats(chatObject) {
    this.props.update(chatObject);
  }

  public render(): JSX.Element {
    const Provider = Context.Provider;
    return (
      <div
        className="main-container"
        style={{ width: this.props.fit && "100%" }}
      >
        <Helmet>
          <meta
            name="theme-color"
            content={this.props.settings.sendBubbleColor.toString()}
          />
        </Helmet>
        <Provider
          value={{
            chatStore: this.props.store,
            updateChatStore: this.updateChats.bind(this),
            sendTextEvent: this.props.onText,
            isRecieving: this.props.isRecieving
              ? this.props.isRecieving
              : false,
            isLoading: this.props.isLoading ? this.props.isLoading : false,
            style: this.props.settings
          }}
        >
          <Header />
          <ChatView />
          <MessageBox />
        </Provider>
      </div>
    );
  }

  public componentDidMount() {
    const body: HTMLCollectionOf<HTMLElement> = document.getElementsByTagName(
      "body"
    );
    body[0].style.backgroundColor = this.props.settings.desktopBackground;
    if (window.innerWidth > 320 && this.props.draggable) {
      interact(".main-container")
        .resizable({
          // resize from all edges and corners
          edges: { left: true, right: true },

          modifiers: [
            // keep the edges inside the parent
            interact.modifiers.restrictEdges({
              outer: "parent",
              endOnly: true
            }),

            // minimum size
            interact.modifiers.restrictSize({
              min: { width: 500, height: 200 }
            })
          ],

          inertia: true
        })
        .on("resizemove", function(event) {
          var target = event.target,
            x = parseFloat(target.getAttribute("data-x")) || 0,
            y = parseFloat(target.getAttribute("data-y")) || 0;

          // update the element's style
          target.style.width = event.rect.width + "px";
          target.style.height = event.rect.height + "px";

          // translate when resizing from top or left edges
          x += event.deltaRect.left;
          y += event.deltaRect.top;

          target.style.webkitTransform = target.style.transform =
            "translate(" + x + "px," + y + "px)";
        });
    }
  }
}

export default MainContainer;
