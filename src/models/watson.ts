import axios from "axios";

const BASE_URL = "https://deolu-bot-backend.herokuapp.com";
if (localStorage.getItem("token")) {
}

interface messageResponse {
  reply?: string | Array<string>;
  context?: object;
}

interface startResponse {
  token?: string;
}
export const sendMessage = message => {
  try {
    return axios
      .post<messageResponse>(`${BASE_URL}/message`, {
        message,
        context: localStorage.getItem("context")
          ? JSON.parse(localStorage.getItem("context"))
          : undefined
      })
      .then(resp => {
        localStorage.setItem("context", JSON.stringify(resp.data.context));
        return resp.data.reply;
      });
  } catch (e) {
    console.log(e);
  }
};

export const startSession = payload => {
  try {
    axios
      .post<startResponse>(`${BASE_URL}/start`, payload)
      .then(resp => {
        localStorage.setItem("token", resp.data.token);
      })
      .catch(e => {
        console.warn(e);
      });
    axios.defaults.headers["Authorization"] = `Bearer ${localStorage.getItem(
      "token"
    )}`;
  } catch (e) {}
};
