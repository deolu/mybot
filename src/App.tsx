import React, { useEffect } from "react";
import Container from "./components/fulscreen";
import "./desktop.scss";
import { useState } from "react";
import "./mobile.scss";
import { sendMessage, startSession } from "./models/watson";
// Demo setting
const style = {
  header: {
    title: "DEOLU",
    image: "",
    align: "",
    backgroundColor: "",
    textColor: "",
    fontFamily: "Red Hat Display"
  },
  messageBar: {
    backgroundColor: "white",
    textColor: "#424242",
    fontFamily: "Red Hat Display"
  },
  headerBackground: "",
  headerTextColor: "",
  desktopBackground: "whitesmoke",
  chatViewBackground: "white",
  headerColor: "",
  sendBubbleColor: "#F5EEE6",
  sendBubbleTextColor: "#1C1D24",
  recieveBubbleColor: "#EA2340",
  recieveBubbleTextColor: "white",
  fontFamily: "Red Hat Display"
};

/*
for the event listenters 
- onText => when the user sends a message 
-
*/

class App extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = { store: [] };
  }
  componentWillMount() {
    startSession({ name: "deolu" });
    localStorage.removeItem("context");
  }

  onTextChange(extra) {
    sendMessage(extra.message).then(reply => {
      this.setState(({ store }) => ({
        store: [...store, { message: reply, type: "recieve" }]
      }));
    });
  }
  componentWillUnmount() {
    localStorage.removeItem("context");
  }
  public UpdateChatStore(value) {
    this.setState({ ...this.state, store: [...this.state.store, value] });
  }
  render() {
    return (
      <Container
        draggable
        store={this.state.store}
        update={this.UpdateChatStore.bind(this)}
        fitParent
        settings={style}
        onText={this.onTextChange.bind(this)}
      />
    );
  }
  componentDidMount() {}
}
export default App;
