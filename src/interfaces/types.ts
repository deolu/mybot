export interface IProps {
  //   style?: style;
  store:Array<any>; 
  fit?:boolean;
  isLoading?: boolean;
  isRecieving?: boolean;
  fitParent?: boolean;
  update?: (message:any) => any;
  onText?: (message: any) => any;
  settings?: style;
  draggable?: boolean;
}

export interface bubble {
  icon?: string;
  active?: boolean;
}

interface messageBar {
  backgroundColor?: string;
  textColor?: string;
  fontFamily?: string;
}
export interface style {
  chatViewBackground?: string;
  desktopBackground?: string;
  headerTextColor?: string;
  headerColor?: string;
  sendBubbleColor?: string;
  sendBubbleTextColor?: string;
  recieveBubbleColor?: string;
  recieveBubbleTextColor?: string;
  fontFamily?: string;
  header?: header;
  messageBar?: messageBar;
}
interface header {
  title: string;
  image?: string;
  align: string;
  fontFamily?: string;
  textColor?: string;
  headerBackground?: string;
}

export interface chatMessage {
  message: string;
  time: string; 
}

export interface storeTypes {
  chatStore: Array<any>;
  updateChatStore: () => any;
  sendTextEvent?: (data: chatMessage) => any;
  isLoading?: boolean;
  isRecieving?: boolean;
  style: style;
}
