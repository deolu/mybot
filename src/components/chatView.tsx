import * as React from "react";
import * as ReactDom from "react-dom";
import Context from "./chatStoreContext";
import { useEffect, useContext } from "react";
import { storeTypes } from "../interfaces/types";
interface IChatBubble {
  type?: "send" | "recieve";
  children?: any;
}

const Chatbubble = React.forwardRef((props: any, ref: any) => {
  const { style } = useContext(Context);
  useEffect(() => {
    ref.current.scrollIntoView({ behavior: "smooth" });
  }, []);
  if (props.type == "send") {
    return (
      <div className="chat-bubble-container-send-zxx" ref={ref}>
        <div
          className="chat-bubble-send-zxx"
          style={{
            backgroundColor: style.sendBubbleColor,
            color: style.sendBubbleTextColor,
            fontFamily: style.fontFamily
          }}
        >
          {props.children}
        </div>
      </div>
    );
  } else if (props.type == "recieve") {
    return (
      <div className="chat-bubble-container-recv-zxx" ref={ref}>
        <div
          className="chat-bubble-recv-zxx"
          style={{
            backgroundColor: style.recieveBubbleColor,
            color: style.recieveBubbleTextColor,
            fontFamily: style.fontFamily
          }}
        >
          {props.children}
        </div>
      </div>
    );
  } else {
    return (
      <div className="chat-bubble-container-recv-zxx" ref={ref}>
        <div
          className="chat-bubble-recv-loading-zxx"
          style={{
            backgroundColor: style.recieveBubbleColor,
            color: style.recieveBubbleTextColor,
            fontFamily: style.fontFamily
          }}
        >
          <div className="balls">
            <div style={{ backgroundColor: style.recieveBubbleTextColor }} />
            <div style={{ backgroundColor: style.recieveBubbleTextColor }} />
            <div style={{ backgroundColor: style.recieveBubbleTextColor }} />
          </div>
        </div>
      </div>
    );
  }
});

class ChatView extends React.Component<any, any> {
  ChatBubble: any = React.createRef();
  public render(): JSX.Element {
    const { chatViewBackground } = this.context.style;
    const { chatStore, isLoading, isRecieving , style } = this.context;
    if (isRecieving) {
      return (
        <div className="chat-view-zxx" style={{ display: "flex ", flexDirection: "row", alignItems: "center", justifyContent: "center" ,backgroundColor: (chatViewBackground != "" ? chatViewBackground : "whitesmoke") }}>
          <div className="loader-zxx" style={{borderTopColor :style.recieveBubbleColor}}></div> 
        </div>
      );
    }
    // come back here
    return (
      <div
        className="chat-view-zxx"
        style={{ backgroundColor: (chatViewBackground != "" ? chatViewBackground : "whitesmoke") }}
      >
        <div>
          {chatStore.map((each, i) => {
            return (
              <Chatbubble key={i} ref={this.ChatBubble} type={each.type}>
                {each.message}
              </Chatbubble>
            );
          })}
          {isLoading && <Chatbubble ref={this.ChatBubble} />}
        </div>
      </div>
    );
  }
}

ChatView.contextType = Context;

export default ChatView;
