import * as React from "react";
import { storeTypes } from "../interfaces/types";



const StoreContext = React.createContext<storeTypes>({
    chatStore: [],
    updateChatStore: () => {},
    isLoading: false,
    sendTextEvent : ()=>{},
    style: { }
});

export default StoreContext;
