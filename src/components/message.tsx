import * as React from "react";
import Context from "./chatStoreContext";
import { useContext } from "react";
import { symbol } from "prop-types";

const MessageBox = props => {
  let inputObject: any = React.createRef();
  const updateStore = (updaterFunction: any) => {
    updaterFunction({ message: inputObject.value, type: "send" });
    if(sendTextEvent){
      sendTextEvent({ message: inputObject.value , time : new Date().toISOString() })
    }
    inputObject.value = "";
    inputObject.focus();
  };
  const { updateChatStore, style, sendTextEvent } = useContext(Context);
  return (
    <div
      className="message-box-zxx"
      style={{
        backgroundColor:
          style.messageBar && style.messageBar.backgroundColor
            ? style.messageBar.backgroundColor
            : "white",
        fontFamily: style.messageBar && style.messageBar.fontFamily ? style.messageBar.fontFamily : ""
      }}
    >
      <form>
        <div style={{ position: "relative", width: "70%" }}>
          <input
            type="text"
            name="Message"
            className="input-zxx"
            style={{
              color:
                style.messageBar && style.messageBar.textColor
                  ? style.messageBar.textColor
                  : "#424242"
            }}
            ref={el => (inputObject = el)}
            placeholder="Enter a Message"
          />
          <span style={{ backgroundColor: style.sendBubbleTextColor }} />
        </div>
        <button
          role="submit"
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            padding: "12px"
          }}
          className="send-button-zxx"
          onClick={e => {
            e.preventDefault();
            updateStore(updateChatStore);
          }}
        >
          <svg
            id="Capa_1"
            xmlns="http://www.w3.org/2000/svg"
            width="auto"
            height="auto"
            viewBox="0 0 535.5 535.5"
          >
            <polygon
              fill={style.sendBubbleTextColor}
              points="0,497.25 535.5,267.75 0,38.25 0,216.75 382.5,267.75 0,318.75"
              id="send"
            />
          </svg>
        </button>
      </form>
    </div>
  );
};

export default MessageBox;
